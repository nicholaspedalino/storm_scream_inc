﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using patterns;

namespace venga
{
    public class WorldStateManager : SingletonBehavior<WorldStateManager>
    {
        //world gravity
        public float gravity = -1f;

        protected override void OnAwake()
        {
            Physics.gravity = new Vector3(0, gravity, 0);
        }
    }
}
