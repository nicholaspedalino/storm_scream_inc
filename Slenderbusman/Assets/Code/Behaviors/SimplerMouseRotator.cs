﻿using UnityEngine;
using System.Collections;

namespace venga
{
    public class SimplerMouseRotator : VengaBehaviour
    {
        public float speed;
        public float easing;

        private Vector2 mod;
        private float screenXYRatio;

        protected override void OnAwake()
        {
            //get screen ratio so X and Y rotations happen at same speed
            screenXYRatio = Screen.width / Screen.height;
        }

        protected override void OnUpdate(float dt)
        {
            //@TODO: OnScreenChangeSize callback in InputManager
            screenXYRatio = Screen.width / Screen.height;


            if (screenXYRatio >= 1.0f)
            {//if the width of the screen is larger than the height

                //ease between current rotation and the mouse input
                mod.x = Mathf.Lerp(mod.x, -Input.GetAxis("Mouse Y") * speed * screenXYRatio * dt, easing);
                mod.y = Mathf.Lerp(mod.y, Input.GetAxis("Mouse X") * speed * dt, easing);
            }
            else
            {//if the height of the screen is larger than the width

                //ease between current rotation and the mouse input
                float screenYXRatio = Screen.height / Screen.width;
                mod.x = Mathf.Lerp(mod.x, -Input.GetAxis("Mouse Y") * speed * dt, easing);
                mod.y = Mathf.Lerp(mod.y, Input.GetAxis("Mouse X") * speed * screenYXRatio * dt, easing);
            }

            //look around
            transform.Rotate(mod.x, mod.y, 0.0f);
        }
    } 
}
