﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace venga
{
    public class FreefallControlBehavior : VengaBehaviour
    {
        public Transform rootController;
        private Vector3 controlAxisVelocity;

        [Header("'Kinematics'")]
        [Tooltip("evaluate this from rest -> max speed")]
        public float acceleration = 2f;

        //[Tooltip("How fast?")]
        //public float maxSpeed;

        //component references
        public Rigidbody rb;

        private bool b_IsMoving = false;

        //public Camera cam;
        protected override void OnAwake()
        {
            //rb = GetComponent<Rigidbody>();
            controlAxisVelocity = rb.velocity = Vector2.zero;

            InputManager.Instance.OnHorizontalVerticalDown += OnXZDown;
            InputManager.Instance.OnHorizontalVerticalUp += OnXZUp;
        }

        protected override void OnUpdate(float dt)
        {
            if (b_IsMoving)
            {
                Vector2 inputVel = InputManager.Instance.horizontalVerticalAxis;
                controlAxisVelocity = new Vector3(inputVel.x, 0, inputVel.y) * acceleration;
                rb.AddRelativeForce(controlAxisVelocity, ForceMode.Acceleration);
            }

        }

        private void OnXZDown()
        {
            b_IsMoving = true;
        }

        private void OnXZUp()
        {
            b_IsMoving = false;
        }
    }
}
