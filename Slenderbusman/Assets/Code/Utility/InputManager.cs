﻿using patterns;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace venga
{
    public class InputManager : SingletonBehavior<InputManager>
    {
        //<ctrl, alt, shift>
        //public Action<bool, bool, bool> OnConsoleKeyDown;
        //public Action<bool, bool, bool> OnConsoleKeyHeld;
        //public Action<bool, bool, bool> OnConsoleKeyUp;

        #region HorizontalVertical axis
        //just began horizontal or vertical input
        public Action OnHorizontalVerticalDown;
        //just ended horizontal or vertical input
        public Action OnHorizontalVerticalUp;
        [HideInInspector]
        public Vector2 horizontalVerticalAxis = Vector2.zero;
        private Vector2 horizontalVerticalAxisPrevious = Vector2.zero;
        #endregion

        protected override void OnAwake()
        {

        }

        protected override void OnUpdate(float dt)
        {
            bool ctrl = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
            bool alt = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
            bool shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

            horizontalVerticalAxis = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            #region HorizontalVerticalAxis
            //HV axis down
            if (horizontalVerticalAxis.sqrMagnitude >= VengaConstants.c_MinAxisMagnitude &&
                horizontalVerticalAxisPrevious.sqrMagnitude < VengaConstants.c_MinAxisMagnitude &&
                OnHorizontalVerticalDown != null)
            {
                OnHorizontalVerticalDown();
            }

            //HV axis release
            if (horizontalVerticalAxis.sqrMagnitude < VengaConstants.c_MinAxisMagnitude &&
                horizontalVerticalAxisPrevious.sqrMagnitude >= VengaConstants.c_MinAxisMagnitude &&
                OnHorizontalVerticalUp != null)
            {
                OnHorizontalVerticalUp();
            }

            horizontalVerticalAxisPrevious = horizontalVerticalAxis;
#endregion
        }
    }
}