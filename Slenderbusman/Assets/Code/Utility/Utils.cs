﻿using System.Collections.Generic;
using UnityEngine;

namespace venga
{
    public static class Utils
    {

        /// <summary>
        /// this could be a collections ISort implementation
        /// </summary>
        /// <param name="myList"></param>
        /// <param name="order">how many multiples of the length of the list do we want to pick two rand indices and shuffle them?</param>
        public static void Shuffle(this List<string> myList, int order=1)
        {
            order = Mathf.Max(1, order);
            int listLen = myList.Count;
            int len = myList.Count * order;
            int a = 0;
            int b = 0;
            string tmp = "";
            for (int i = 0; i < len; i++)
            {
                //I don't care that it's possible for a and b to be the same.
                a = UnityEngine.Random.Range(0, listLen);
                b = UnityEngine.Random.Range(0, listLen);

                tmp = myList[a];
                myList[a] = myList[b];
                myList[b] = tmp;
            }
        }
    }
}
