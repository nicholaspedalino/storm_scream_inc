﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace venga
{
    public class VengaBehaviour : MonoBehaviour
    {
        private void Awake()
        {
            //register self to manager manager?
            //Debug.Log(this.GetType().ToString() + "::OnAwake()");
            OnAwake();
        }
        protected virtual void OnAwake()
        {
        }

        private void Update()
        {
            //register self to manager manager?
            OnUpdate(Time.deltaTime);
        }
        protected virtual void OnUpdate(float dt)
        {
        }
    }
}
