﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StringManager : MonoBehaviour {
	[Range(2, 50)]
	public int numNodes;
	[Range(.01f, 2f)]
	public float nodeSpacing;
	public Camera cam;

	private Vector3[] nodes;
	private LineRenderer line;
	private bool aboveString;
	private int nearestNode;

	// Use this for initialization
	void Start () {
		nodes = new Vector3[numNodes];
		for (int i = 0; i < numNodes; i++) {
			nodes [i] = new Vector3 (i * nodeSpacing, 0f, 0f);
		}

		line = GetComponent<LineRenderer> ();
		line.SetVertexCount (numNodes);
		line.SetPositions (nodes);
	}
	
	// Update is called once per frame
	void Update () 
    {
		Vector3 mousePosWorld = cam.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 10f));
		if (Input.GetMouseButtonDown(0)) {
			if (mousePosWorld.y > 0f) {
				aboveString = true;
			} else {
				aboveString = false;
			}
		}

		if (Input.GetMouseButton(0)) {
			float roundedX = Mathf.Ceil (mousePosWorld.x * (1 / nodeSpacing)) / (1 / nodeSpacing);
			nearestNode = Mathf.RoundToInt (roundedX / nodeSpacing);
				if (nearestNode > 0 && nearestNode < numNodes - 1) {
					
					if ((mousePosWorld.y < 0 && aboveString) || (mousePosWorld.y > 0 && !aboveString)) 
					{
						nodes [nearestNode].y = mousePosWorld.y;
					}

				}
				for (int i = 0; i < numNodes; i++) {
					if (i != nearestNode) 
					{
						nodes [i].y /= 1.2f;
					}
				}
			} else {
				for (int i = 0; i < numNodes; i++) {
					nodes [i].y /= 1.5f;
				}
			}

		line.SetPositions (nodes);
	}
}
