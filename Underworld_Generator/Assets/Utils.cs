﻿using UnityEngine;
using System.Collections;

public static class Utils
{ 
    public static void CreateDebugSphereAt(Vector3 pos, float scale=1)
    {
        GameObject newGo = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        newGo.transform.position = pos;
        newGo.transform.localScale = new Vector3(scale, scale, scale);
    }
    
    public static Vector3 GetRandomUnitVectorAroundYAxis()
    {
        float theta = UnityEngine.Random.Range(0.0f, Mathf.PI * 2.0f);
        return (new Vector3(Mathf.Cos(theta), 0.0f, Mathf.Sin(theta)));
    }

}
