﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class DungeonGenerator : MonoBehaviour
{
    void Start()
    {
        //Room newRoom = new Room();
        //newRoom.CreateJaggedDonutRoom(3.0f, 15.0f, 12, 24, 0.4f, 2.0f);
        //for (int i = 0; i < 100; i++)
        //newRoom.CreateLumpyCircleRoom(10.0f, 100, 1.0f, 2.0f, 0.0f, 0.1f);
        //newRoom.CreateLumpyCircleRoom(16.0f, 64, 13.0f, 14.0f, 1.45f, 1.5f);

        Floor newFloor = new Floor();
        newFloor.GenerateFloor(30);
    }

    void FixedUpdate()
    {
        //Debug.Break();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FLOORS (clusters of rooms, hallways, paths, areas, etc)
public class Floor
{
    private List<GameObject> mRoomObjects;   //store all room gameobjects
    private List<Room> mRooms;               //store all room gameobjects' Room components
    private List<Hallway> mHallways;

    //confusing: //private int mNumRooms;//total number of rooms after generation
    private int mRoomCount;//current count of rooms generated (used during generation)

    public Floor()
    {
        mRoomObjects = new List<GameObject>();
        mRooms = new List<Room>();
        mHallways = new List<Hallway>();

        mRoomCount = 0;
    }

    public void GenerateFloor(int numRooms)
    {
        //create spawn room
        CreateUnconnectedRoom(RoomType.SPAWN, UnityEngine.Random.Range(12.0f, 26.0f), Vector3.zero);

        GameObject.Find("FPSController").transform.position = new Vector3(mRooms[0].transform.position.x, 1, mRooms[0].transform.position.z);


        float newRadius = 0.0f;
        int giveUpCounter = 0;
        for (int i = 0; i < numRooms-1; i++)
        {
            //create another room that is attached to the previous room with a hallway in a random(valid) direction
            newRadius = UnityEngine.Random.Range(18.0f, 26.0f);

            //check for errors.. if we couldn't make a room, try and make a room adjoining to the previous room
            //if we absolutely cannot make a room, give up
            while (i >= mRooms.Count)
            {
                i--;
            }

            if(CreateNewConnectedRoom(RoomType.NONE, mRooms[i], newRadius) != 0)
            {
                giveUpCounter++;
                if (giveUpCounter>30)
                {
                    return;
                }
            }
            else
            {
                giveUpCounter = 0;
            }
        }
        #region fucky
        ////pick a random direction for the first room's door
        //Vector3 newRoomDirection = Utils.GetRandomUnitVectorAroundYAxis();
        ////create a door here
        //int randDoorWidthStart = UnityEngine.Random.Range(3, 12);
        //int randDoorWidthNext = UnityEngine.Random.Range(3, 12);
        ////mRooms[0].CreateDoor(newRoomDirection, randDoorWidthStart);
        //
        ////make sure the door normal is actually outward facing and otherwise correct (it is)
        ////Debug.DrawLine(mRooms[0].doors[0].GetVerts()[randDoorWidth/2], mRooms[0].doors[0].doorNormalOutward * 10, Color.white, 100);
        //
        //float newRoomRadius = UnityEngine.Random.Range(8.0f, 22.0f);
        //Room newRoom = CreateNewRoom(mRooms[0], newRoomDirection, randDoorWidthStart, randDoorWidthNext, newRoomRadius);
        //
        //Hallway connectingHallway = new Hallway(newRoom.doors[0], mRooms[0].doors[0], 50);
        //connectingHallway.GenerateWalls(1.0f);
        //mHallways.Add(connectingHallway);
        //
        //for (int i = 1; i < numRooms-1; i++)
        //{
        //    //pick a random direction
        //    newRoomDirection = Utils.GetRandomUnitVectorAroundYAxis();
        //
        //    //create a door here
        //    randDoorWidthStart = UnityEngine.Random.Range(3, 12);
        //    randDoorWidthNext = UnityEngine.Random.Range(3, 12);
        //
        //    newRoomRadius = UnityEngine.Random.Range(8.0f, 22.0f);
        //    
        //    Room nextRoom = CreateNewRoom(mRooms[i], newRoomDirection, randDoorWidthStart, randDoorWidthNext, newRoomRadius);
        //    nextRoom.CreateDoor(newRoomDirection, randDoorWidthStart);
        //
        //    Hallway nextConnectingHallway = new Hallway(nextRoom.doors[nextRoom.doors.Count-1], mRooms[i+1].doors[mRooms[i].doors.Count-1], 50);
        //    nextConnectingHallway.GenerateWalls(1.0f);
        //    mHallways.Add(nextConnectingHallway);
        //}
        #endregion fucky
    }

    private int CreateNewConnectedRoom(RoomType roomType, Room room, float roomRadius)
    {
        Vector3 newRoomDirection;// = Utils.GetRandomUnitVectorAroundYAxis();

        bool hasSuccessfullyCreatedNewRoom = false;
        Vector3 newRoomPosition = Vector3.zero;

        int i = 0;
        while (!hasSuccessfullyCreatedNewRoom)
        {
            //try a random direction
            newRoomDirection = Utils.GetRandomUnitVectorAroundYAxis();

            //get the position that this new room would be in given this new direction
            //radiusPrevious + radiusNew + a bit of extra space
            newRoomPosition = newRoomDirection * (roomRadius + room.GetBoundingRadius() + UnityEngine.Random.Range(1.0f, 20.0f));

            //add previous room position to new position
            newRoomPosition += room.transform.position;
            Room newRoom = CreateUnconnectedRoom(roomType, roomRadius, newRoomPosition);

            //if the new room is not intersecting any other rooms
            if (!(CheckNewRoomBounds(newRoom)))
            {
                hasSuccessfullyCreatedNewRoom = true;
                newRoom.CreateDoor(-newRoomDirection, 5);
                room.CreateDoor(newRoomDirection, 5);

                //create hallway between this room and the previous room
                Hallway nextConnectingHallway = new Hallway(newRoom.doors[newRoom.doors.Count - 1], room.doors[room.doors.Count - 1], 50);
                nextConnectingHallway.GenerateWalls(1.0f);
                mHallways.Add(nextConnectingHallway);
                return 0;
            }
            else
            {
                mRooms.Remove(newRoom);
                GameObject.Destroy(newRoom.gameObject);
            }

            //if the new room is just not going to happen
            if (i > 100)
            {
                return -1;
            }
            i++;
        }

        return 0;
        
    }

    #region fucky#5
    /// <summary>
    /// attempt to create a door in a given direction from the origin of this room,
    /// followed by another room placed in that direction,
    /// 
    /// for a given theta degree of imperfection -- BRAIN CURSOR
    /// 
    /// </summary>
    /// <param name="startRoom">the room we will be branching off of</param>
    /// <param name="direction">which way will we attempt to create a room?</param>
    /// <param name="doorWidthStart">how many verts will the door on the first room be?</param>
    /// <param name="doorWidthEnd">how many verts will the door on the new room be?</param>
    /// <param name="newRoomRadius">radius of the new room</param>
    //public Room CreateNewRoom(Room startRoom, Vector3 direction, int doorWidthStart, int doorWidthEnd, float newRoomRadius)
    //{
    //    Vector3 startRoomPos = startRoom.gameObject.transform.position;
    //    float startRoomRadius = startRoom.GetBoundingRadius();
    //
    //    float additionalNewRoomDistance = UnityEngine.Random.Range(4.0f, 12.0f);//could make this relative to radii, but let's do this first
    //    Vector3 nextRoomPos = startRoomPos + (direction * (startRoomRadius + newRoomRadius + additionalNewRoomDistance));
    //
    //    Room newRoom = CreateUnconnectedRoom(RoomType.NONE, newRoomRadius, nextRoomPos);
    //    newRoom.CreateDoor(direction, doorWidthEnd);
    //
    //    return newRoom;
    //}
    #endregion fucky#6

    private Room CreateUnconnectedRoom(RoomType type, float radius, Vector3 origin)
    {
        GameObject newRoomObj = new GameObject();
        newRoomObj.name = "room_" + mRoomCount.ToString();
        Room newRoomComponent = newRoomObj.AddComponent<Room>();

        //none of these work quite yet...
        RoomShape randShape = (RoomShape)(UnityEngine.Random.Range(0, (int)RoomShape.TOTAL_IMPLEMENTED));

        switch (randShape)
        {
            case RoomShape.JAGGED_CIRCLE:
                newRoomComponent.CreateJaggedCircleRoom(radius,  //radius
                                                       100,     //num verts
                                                       UnityEngine.Random.Range(1.0f, 5.25f));   //jagged factor
                break;
            case RoomShape.JAGGED_DONUT:
                newRoomComponent.CreateJaggedDonutRoom( radius / UnityEngine.Random.Range(4.0f, 12.0f), //inner radius
                                                        radius,     //outer radius
                                                        10,         //num inner verts
                                                        100,        //num outer verts
                                                        UnityEngine.Random.Range(0.0f, 1.5f),    //jagged factor inner
                                                        UnityEngine.Random.Range(1.0f, 5.25f));   //jagged factor outer
                break;
            case RoomShape.LUMPY_CIRCLE:
                newRoomComponent.CreateLumpyCircleRoom(radius,  //radius
                                                       UnityEngine.Random.Range(75, 200),       //num verts
                                                       UnityEngine.Random.Range(1.0f, 2.25f),   //min lump factor
                                                       UnityEngine.Random.Range(3.0f, radius),    //max lump factor
                                                       UnityEngine.Random.Range(0.35f, 0.45f),  //min lump increment
                                                       UnityEngine.Random.Range(0.55f, 0.75f),  //max lump increment
                                                       UnityEngine.Random.Range(0.25f, 0.5f));  //normalized smoothing
                break;
            case RoomShape.CIRCLE:
                newRoomComponent.CreateCircleRoom(radius, (UnityEngine.Random.Range(3, 50)) / 10 + 3);
                break;
            case RoomShape.RECTANGLE:
                float minRectDimension = 5.0f;
                if (radius < minRectDimension)
                {
                    radius = minRectDimension * 2.0f;
                }
                newRoomComponent.CreateRectangleRoom(UnityEngine.Random.Range(minRectDimension, radius), UnityEngine.Random.Range(minRectDimension, radius));
                break;
            case RoomShape.VERTEX_ARRAY_DEFINED:
                break;
            default:
                break;
        }
        newRoomComponent.SetRoomType(type);
        newRoomObj.transform.position = origin;

        mRoomObjects.Add(newRoomObj);
        mRooms.Add(newRoomComponent);
        mRoomCount++;

        return newRoomComponent;
    }

    //check whether this room is intersecting any other room
    private bool CheckNewRoomBounds(Room room)
    {
        Vector3 roomPos = room.gameObject.transform.position;
        float roomRadius = room.GetBoundingRadius();
        for (int i = 0; i < mRooms.Count; i++)
        {
            if (mRooms[i] != null && room.gameObject.name != mRooms[i].name)
            {
                if (CheckBoundingCircleIntersection(roomPos, roomRadius,
                                                    mRooms[i].transform.position, mRooms[i].GetComponent<Room>().GetBoundingRadius()))
                {
                    //we've hit an intersection
                    return true;
                }
            }
        }
        //no intersection
        return false;
    }

    private bool CheckBoundingCircleIntersection(Vector3 centerPosA, float radiusA, Vector3 centerPosB, float radiusB)
    {
        return (Vector3.Distance(centerPosA, centerPosB) < radiusA + radiusB);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HALLWAYS

public class Hallway
{
    private Door mDoor1;
    private Door mDoor2;

    int mNumWallVerts;
    public Vector3[] mSide1Verts;
    public Vector3[] mSide2Verts;

    private GameObject[] mWall1Objects;
    private GameObject[] mWall2Objects;

    private GameObject mParentObject;

    public float wallHeight = 10.0f;

    public Hallway(Door door1, Door door2, int numWallVerts)
    {
        mDoor1 = door1;
        mDoor2 = door2;

        mSide1Verts = new Vector3[numWallVerts + 2];//+2 for the door vert connections
        mSide2Verts = new Vector3[numWallVerts + 2];

        mWall1Objects = new GameObject[numWallVerts + 2];
        mWall2Objects = new GameObject[numWallVerts + 2];

        mParentObject = new GameObject();
        mParentObject.name = "Hallway";

        //average position of the door centerpoints will be the parent position of the hallway
        mParentObject.transform.position = ((mDoor1.GetVerts()[0] + mDoor1.GetVerts()[mDoor1.GetVerts().Length - 1]) / 2.0f) +
                                           ((mDoor2.GetVerts()[0] + mDoor2.GetVerts()[mDoor2.GetVerts().Length - 1]) / 2.0f) / 2.0f;
        
        mNumWallVerts = numWallVerts;
    }

    public void GenerateWalls(float curviness)
    {
        Vector3[] door1Verts = mDoor1.GetVerts();
        Vector3[] door2Verts = mDoor2.GetVerts();
        
        mSide1Verts[0] = door1Verts[door1Verts.Length - 1];
        mSide1Verts[mSide1Verts.Length - 1] = door2Verts[0];

        mSide2Verts[0] = door1Verts[0];
        mSide2Verts[mSide2Verts.Length - 1] = door2Verts[door2Verts.Length - 1];

        for (int i = 1; i < mNumWallVerts+1; i++)
        {
            mSide1Verts[i] = Vector3.Lerp(door1Verts[door1Verts.Length - 1], door2Verts[0], (float)i / (float)mNumWallVerts);
            mSide2Verts[i] = Vector3.Lerp(door1Verts[0], door2Verts[door2Verts.Length - 1], ((float)i / (float)mNumWallVerts));
        }

        GenerateWallGameObjects();
        GenerateWallGameObjects();
    }

    private void GenerateWallGameObjects()
    {
        /////////////////////////////////////////////////////////////////////////////////////////////
        //generate wall 1
        float xScale = (Vector3.Magnitude(mSide1Verts[1] - mSide1Verts[0]));

        Vector3 lookVector = Vector3.zero;

        //create walls
        for (int i = 0; i < mNumWallVerts+1; i++)
        {
            //new cube
            GameObject newWall = GameObject.CreatePrimitive(PrimitiveType.Cube);
            newWall.transform.parent = mParentObject.transform;

            //all but the last i
            Vector3 diff = mSide1Verts[i] - mSide1Verts[i + 1];

            //new position is the midpoint between this vert and the next
            newWall.transform.position = (mSide1Verts[i] + mSide1Verts[i + 1]) / 2.0f;

            //set the scale
            xScale = (Vector3.Magnitude(diff));

            //which way is the wall facing?
            lookVector = new Vector3(-diff.z, 0, diff.x);

            newWall.name = "wall" + i;

            //set new scale
            newWall.transform.localScale = new Vector3(xScale, wallHeight, 0.01f);

            //set new rotation
            if(lookVector != Vector3.zero)
            newWall.transform.rotation = Quaternion.LookRotation(lookVector);

            //add to array
            mWall1Objects[i] = newWall;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////
        //generate wall 2
        xScale = (Vector3.Magnitude(mSide2Verts[1] - mSide2Verts[0]));

        //create walls
        for (int i = 0; i < mNumWallVerts+1; i++)
        {
            //new cube
            GameObject newWall = GameObject.CreatePrimitive(PrimitiveType.Cube);
            newWall.transform.parent = mParentObject.transform;

            //all but the last i
            Vector3 diff = mSide2Verts[i] - mSide2Verts[i + 1];

            //new position is the midpoint between this vert and the next
            newWall.transform.position = (mSide2Verts[i] + mSide2Verts[i + 1]) / 2.0f;

            //set the scale
            xScale = (Vector3.Magnitude(diff));

            //which way is the wall facing?
            lookVector = new Vector3(-diff.z, 0, diff.x);

            newWall.name = "wall" + i;

            //set new scale
            newWall.transform.localScale = new Vector3(xScale, wallHeight, 0.01f);

            //set new rotation
            if(lookVector != Vector3.zero)
            newWall.transform.rotation = Quaternion.LookRotation(lookVector);

            //add to array
            mWall2Objects[i] = newWall;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ROOMS AND DOORS

public class Door
{//stub
    private Vector3[] mVerts;
    public Vector3 doorNormalOutward;//normal facing away from the room
    public Vector3 doorNormalInward;//normal facing into the room

    public int[] wallVertsIndices;//index locations in wallVerts of all door verts

    public Door(Vector3[] verts, int[] indicesInWallVerts)
    {
        wallVertsIndices = indicesInWallVerts;
        mVerts = verts;
        GenerateDoorNormals();
    }
    public Vector3[] GetVerts() { return mVerts; }
    
    private void GenerateDoorNormals()
    {
        Vector3 normal = mVerts[0] - mVerts[mVerts.Length - 1];
        doorNormalOutward = new Vector3(-normal.z, 0, normal.x);
        normal = mVerts[mVerts.Length - 1] - mVerts[0];
        doorNormalInward = new Vector3(normal.z, 0, -normal.x);
    }
}

public enum RoomType
{
    NONE = 0,
    SPAWN,
    TOTAL
}

public enum RoomShape
{
    LUMPY_CIRCLE,
    TOTAL_IMPLEMENTED,
    CIRCLE,
    RECTANGLE,
    JAGGED_CIRCLE,
    JAGGED_DONUT,
    VERTEX_ARRAY_DEFINED
}

public class Room : MonoBehaviour
{
    public Vector3[] wallVerts;//not rotated!
    public float wallHeight = 10.0f;

    public GameObject[] mWalls;
    public List<Door> doors;

    private float boundingRadius;
    public float GetBoundingRadius() { return boundingRadius; }

    private RoomType mRoomType = RoomType.NONE;
    public void SetRoomType(RoomType rt) { mRoomType = rt; }
    public RoomType GetRoomType() { return mRoomType; }

    private RoomShape mRoomShape;
    public RoomShape GetRoomShape() { return mRoomShape; }

    public Room()
    {
        doors = new List<Door>();
    }

    public void CreateDoor(Vector3 directionFromOrigin, int doorWidth)
    {
        Vector3 approxDoorOrigin = (directionFromOrigin * boundingRadius) + transform.position;
        int numWalls = mWalls.Length;

        float nearestSquaredDistance = float.MaxValue;
        float currentSquaredDistance = 0.0f;

        int nearestWallIndex = 0;
        
        for (int i = 0; i < numWalls; i++)
        {
            currentSquaredDistance = (approxDoorOrigin - mWalls[i].transform.position).magnitude;
            if (currentSquaredDistance < nearestSquaredDistance)
            {
                nearestSquaredDistance = currentSquaredDistance;
                nearestWallIndex = i;
            }
        }

        CreateDoor(nearestWallIndex, doorWidth);

        //Utils.CreateDebugSphereAt(mWalls[nearestWallIndex].transform.position, 3.0f);
    }

    public void CreateDoor(int wallIndex, int doorWidth)
    {
        int currentIndex = wallIndex - doorWidth / 2;
        int numWallVerts = mWalls.Length;

        Vector3[] wallVerts = new Vector3[doorWidth];
        int[] wallIndices = new int[doorWidth];

        for (int i = 0; i < doorWidth; i++)
        {
            if (currentIndex < 0)
            {
                currentIndex += numWallVerts;
            }
            if (currentIndex >= numWallVerts)
            {
                currentIndex -= numWallVerts;
            }
            if (i == 0)
            {
                wallVerts[i] = mWalls[currentIndex].transform.TransformPoint(new Vector3(.5f, 0, 0));
                //wallVerts[i] = mWalls[currentIndex].transform.position;
                wallIndices[i] = currentIndex;
            }
            else if (i == doorWidth-1)
            {
                wallVerts[i] = mWalls[currentIndex].transform.TransformPoint(new Vector3(-.5f, 0, 0));
                //wallVerts[i] = mWalls[currentIndex].transform.position;
                wallIndices[i] = currentIndex;
            }
            else
            {
                wallVerts[i] = mWalls[currentIndex].transform.position;
                wallIndices[i] = currentIndex;
            }

            //dont actually destroy the first and last walls
            //if (i > 0 && i < doorWidth-1)
            //{
                Destroy(mWalls[currentIndex]);
            //}
            currentIndex++;
        }

        Door newDoor = new Door(wallVerts, wallIndices);
        doors.Add(newDoor);
    }

    private bool CheckIfDoorExists(Door door)
    {
        //best cast: if there are no doors yet :)
        if (doors != null && doors.Count > 0)
        {
            //for all doors
            for (int i = 0; i < doors.Count; i++)
            {
                //make sure this isnt just a blank allocated space in doors
                if (doors[i] != null)
                {
                    //for all door indices in the door (param)
                    for (int j = 0; j < door.wallVertsIndices.Length; j++)
                    {
                        //for all door indices in doors[i]
                        for (int k = 0; k < doors[i].wallVertsIndices.Length; k++)
                        {
                            //check equivalence
                            if (door.wallVertsIndices[j] == doors[i].wallVertsIndices[k])
                            {
                                //break out early :)
                                return true;
                            }
                        }
                    }
                }
                //if this spot in the door array isn't allocated, none after this in the array should be -- may need to change this later brain cursor
                else
                {
                    return false;
                }
            }
        }
        //worst case efficiency, but probably most likely to occur as overlapping doors is unlikely. this could be done better by an array of valid verts :(
        return false;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // JAGGED ROOM VERTEX GENERATION
    public void CreateJaggedCircleRoom(float radius, int numPoints, float jaggedFactor)
    {
        mRoomShape = RoomShape.JAGGED_CIRCLE;

        wallVerts = new Vector3[numPoints];
        float angleStep = 0.0f;
        float currentLumpFactor = 0.0f;

        for (int i = 0; i < numPoints; i++)
        {
            angleStep = Mathf.PI * 2.0f * ((float)i / numPoints);

            currentLumpFactor = UnityEngine.Random.Range(-jaggedFactor, jaggedFactor);

            wallVerts[i] = new Vector3(Mathf.Cos(angleStep) * (radius + currentLumpFactor),
                                        0,
                                        Mathf.Sin(angleStep) * (radius + currentLumpFactor));
        }


        float radiusDiff;
        boundingRadius = 0.0f;
        for (int i = 0; i < wallVerts.Length; i++)
        {
            radiusDiff = wallVerts[i].magnitude;

            //we can find our bounding radius here
            boundingRadius = Mathf.Max(boundingRadius, radiusDiff);
        }

        GenerateWalls();
    }

    public void CreateJaggedDonutRoom(float innerRadius, float outerRadius, int numInnerPoints, int numOuterPoints, float innerJaggedFactor, float outerJaggedFactor)
    {
        mRoomShape = RoomShape.JAGGED_DONUT;

        wallVerts = new Vector3[numOuterPoints];
        float angleStep = 0.0f;
        float currentLumpFactor = 0.0f;

        for (int i = 0; i < numOuterPoints; i++)
        {
            angleStep = Mathf.PI * 2.0f * ((float)i / numOuterPoints);

            currentLumpFactor = UnityEngine.Random.Range(-outerJaggedFactor, outerJaggedFactor);

            wallVerts[i] = new Vector3(Mathf.Cos(angleStep) * (outerRadius + currentLumpFactor),
                                        0,
                                        Mathf.Sin(angleStep) * (outerRadius + currentLumpFactor));
        }
        GenerateWalls();

        System.Array.Clear(wallVerts, 0, wallVerts.Length);
        wallVerts = new Vector3[numInnerPoints];

        for (int i = 0; i < numInnerPoints; i++)
        {
            angleStep = Mathf.PI * 2.0f * ((float)i / numInnerPoints);

            currentLumpFactor = UnityEngine.Random.Range(-innerJaggedFactor, innerJaggedFactor);

            wallVerts[i] = new Vector3(Mathf.Cos(angleStep) * (innerRadius + currentLumpFactor),
                                        0,
                                        Mathf.Sin(angleStep) * (innerRadius + currentLumpFactor));
        }


        float radiusDiff;
        boundingRadius = 0.0f;
        for (int i = 0; i < wallVerts.Length; i++)
        {
            radiusDiff = wallVerts[i].magnitude;

            //we can find our bounding radius here
            boundingRadius = Mathf.Max(boundingRadius, radiusDiff);
        }

        GenerateWalls();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // LUMPY ROOM VERTEX GENERATION
    public void CreateLumpyCircleRoom(float radius, int numPoints,float minLumpFactor, float maxLumpFactor, float minLumpIncrement, float maxLumpIncrement, float normalizedSmoothing = 0.25f)
    {
        mRoomShape = RoomShape.LUMPY_CIRCLE;

        //each lump should at minimum be 2 degrees of lumpFactor away
        if (maxLumpIncrement > maxLumpFactor / 2.0f)
        {
            maxLumpIncrement = maxLumpFactor / 2.0f;
        }
        minLumpIncrement = Mathf.Min(minLumpIncrement, maxLumpIncrement);


        wallVerts = new Vector3[numPoints];
        float angleStep = 0.0f;
        float currentLumpFactor = 0.0f;
        float currentLumpIncrement = 0.0f;
        bool negate = false;

        float lumpFactor = UnityEngine.Random.Range(minLumpFactor, maxLumpFactor);
        for (int i = 0; i < numPoints; i++)
        {
            angleStep = Mathf.PI * 2.0f * ((float)i / numPoints);

            currentLumpIncrement = UnityEngine.Random.Range(minLumpIncrement, maxLumpIncrement);

            //most of the generation happens in this if
            if (angleStep < (2.0f * Mathf.PI) - (Mathf.PI / 3.5f))//work numPoints into this somehow...
            {
                if (!negate)
                {
                    currentLumpFactor += currentLumpIncrement;
                }
                else
                {
                    currentLumpFactor -= currentLumpIncrement;
                }

                if (currentLumpFactor > lumpFactor || currentLumpFactor < -lumpFactor)
                {
                    //Debug.Log("create door");
                    lumpFactor = UnityEngine.Random.Range(minLumpFactor, maxLumpFactor);
                    negate = !negate;
                    if (!negate)
                    {
                        currentLumpFactor += currentLumpIncrement;
                    }
                    else
                    {
                        currentLumpFactor -= currentLumpIncrement;
                    }
                }


                wallVerts[i] = new Vector3(Mathf.Cos(angleStep) * (radius + currentLumpFactor),
                                            0,
                                            Mathf.Sin(angleStep) * (radius + currentLumpFactor));
            }
            //if we are winding down (heh) to the final vertices (approx), start easing towards the first point
            else
            {
                float radiiDiff = Vector3.Distance(wallVerts[0], Vector3.zero) - (currentLumpFactor + radius);

                currentLumpIncrement = radiiDiff / (numPoints - i);

                int startI = i;
                for (i = startI; i < numPoints; i++)
                {
                    angleStep = Mathf.PI * 2.0f * ((float)i / numPoints);
                    currentLumpFactor += currentLumpIncrement;
                    wallVerts[i] = new Vector3(Mathf.Cos(angleStep) * (radius + currentLumpFactor),
                                                0,
                                                Mathf.Sin(angleStep) * (radius + currentLumpFactor));
                }
            }
        }

        //second pass of vertices: smooth them out
        float radiusDiff;
        boundingRadius = 0.0f;
        for (int i = 0; i < wallVerts.Length; i++)
        {
            radiusDiff = wallVerts[i].magnitude - radius;
            wallVerts[i] -= wallVerts[i].normalized * (radiusDiff * normalizedSmoothing);

            //we can find our bounding radius here
            boundingRadius = Mathf.Max(boundingRadius, (Mathf.Abs(radiusDiff) + radius));
        }

        GenerateWalls();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CIRCLE ROOM VERTEX GENERATION
    public void CreateCircleRoom(float radius, int numPoints)
    {
        mRoomShape = RoomShape.CIRCLE;

        numPoints = Mathf.Max(numPoints, 3);//fuck off if you pass a value less than 3
        wallVerts = new Vector3[numPoints];

        float angleStep = 0.0f;

        for (int i = 0; i < numPoints; i++)
        {
            angleStep = Mathf.PI * 2.0f * ((float)i / numPoints);
            wallVerts[i] = new Vector3(Mathf.Cos(angleStep) * radius,
                                        0.0f,
                                        Mathf.Sin(angleStep) * radius);
        }
        boundingRadius = radius;
        GenerateWalls();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // RECTANGLE ROOM VERTEX GENERATION
    public void CreateRectangleRoom(float width, float length)
    {
        CreateRectangleRoom(width, length, UnityEngine.Random.Range(0.0f, 360.0f));
    }
    public void CreateRectangleRoom(float width, float length, float rotationDegrees)
    {
        mRoomShape = RoomShape.RECTANGLE;

        boundingRadius = Mathf.Max(width/2.0f, length/2.0f);

        wallVerts = new Vector3[4];

        //calculate non-rotated rectangle verts
        wallVerts[0] = new Vector3(-width / 2.0f, 0.0f,  length / 2.0f);
        wallVerts[1] = new Vector3( width / 2.0f, 0.0f,  length / 2.0f);
        wallVerts[2] = new Vector3( width / 2.0f, 0.0f, -length / 2.0f);
        wallVerts[3] = new Vector3(-width / 2.0f, 0.0f, -length / 2.0f);

        //do this once
        float angleSin = Mathf.Sin(rotationDegrees);
        float angleCos = Mathf.Cos(rotationDegrees);

        //rotate each point about (0, 0, 0)
        for (int i = 0; i < 4; i++)
        {
            Vector3 newVec = new Vector3( wallVerts[i].x * angleCos - wallVerts[i].z * angleSin,
                                          0.0f, 
                                          wallVerts[i].x * angleSin + wallVerts[i].z * angleCos);
            wallVerts[i] = newVec;
        }

        GenerateWalls();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // if you want to just tell me to make a room with an array of vertices... I'll do it... just make sure theyre in the right order.
    // I can't tell what the order you want is lol
    public void CreateRoom(Vector3[] verts)
    {
        mRoomShape = RoomShape.VERTEX_ARRAY_DEFINED;
        wallVerts = verts;
        GenerateWalls();
    }
    

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CREATE THE ACTUAL WALL GAMEOBJECTS -- call this only after mWallVerts has been set.
    private void GenerateWalls()
    {
        int numWallVerts = wallVerts.Length;
        mWalls = new GameObject[numWallVerts];

        float xScale = (Vector3.Magnitude(wallVerts[1] - wallVerts[0]));

        Vector3 lookVector = Vector3.zero;

        //create walls
        for (int i = 0; i < numWallVerts; i++)
        {
            //new cube
            GameObject newWall = GameObject.CreatePrimitive(PrimitiveType.Cube);
            newWall.transform.parent = gameObject.transform;

            //all but the last i
            if (i < numWallVerts-1)
            {
                Vector3 diff = wallVerts[i] - wallVerts[i + 1];

                //new position is the midpoint between this vert and the next
                newWall.transform.position = (wallVerts[i] + wallVerts[i + 1]) / 2.0f;

                //set the scale
                xScale = (Vector3.Magnitude(diff));

                //which way is the wall facing?
                lookVector = new Vector3(-diff.z, 0, diff.x);
            }
            //wrap the last i around to [0]
            else if (i == numWallVerts-1)
            {
                Vector3 diff = wallVerts[i] - wallVerts[0];

                //new position is the midpoint between this vert and the next
                newWall.transform.position = (wallVerts[i] + wallVerts[0]) / 2.0f;

                //set the scale
                xScale = (Vector3.Magnitude(diff));
                
                //which way is the wall facing?
                lookVector = new Vector3(-diff.z, 0, diff.x);
            }
            newWall.name = "wall" + i;

            //set new scale
            newWall.transform.localScale = new Vector3(xScale, wallHeight, 0.01f);

            //set new rotation
            newWall.transform.rotation = Quaternion.LookRotation(lookVector);

            //add to array
            mWalls[i] = newWall;
        }
    }
}

