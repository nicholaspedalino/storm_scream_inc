The project I'm working on now:

I want to make a visual text-adventure where you play as an alchemist selling potions and poisons to adventurers.
I want the plants in your garden to grow in real-time, so I'm making a tree generator.

Right now it just makes a trunk, but I did some fancy vector math to make the trunk grow with the cylinders facing forwards.
Also getting a cylinder to generate correctly was a pain, but it works!*

*Just don't tell Unity to RecalculateNormals or add a mesh collider... It does not like the duplicate vertices I used for wrapping.