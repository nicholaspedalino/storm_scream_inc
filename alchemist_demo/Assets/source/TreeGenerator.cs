﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//cylinder mesh generation reference:: http://dev.doukasd.com/2012/07/customizable-procedural-cylinder-for-unity3d-editor/

public class TreeGenerator : MonoBehaviour
{
    public enum TreeType
    {
        CONIFEROUS,
        DECIDUOUS,
    }

    [Header("Tree Prefabs")]
    public GameObject _wood;
    public Material woodMaterial;

    //----------------------------------------------------------------------------------------------------------------
    // trunk data
    [Tooltip("Coniferous = pine tree, Deciduous = maple tree")]
    public TreeType treeType = TreeType.CONIFEROUS;

    [Tooltip("radius in world coordinates of the base of the trunk")]
    public float trunkBaseRadius = 3.0f;

    [Tooltip("how many vertices will be in each ring of the trunk cylinder?")]
    public int cylinderResolution = 12;

    [Tooltip("how tall, in world space, will the trunk be?")]
    public float trunkHeight = 12.0f;

    [Tooltip("How many degrees will the trunk bend?")]
    public float trunkTotalThetaDegrees = 10.0f;

    //----------------------------------------------------------------------------------------------------------------
    // constants
    private const float DEGREES_PER_VERTICAL_SLICE = 2.0f;

    //----------------------------------------------------------------------------------------------------------------
    // mesh generation
    private MeshFilter mTrunkMeshFilter;
    private Mesh mTrunkMesh;
    private MeshRenderer mTrunkMeshRenderer;

    void Start()
    {
        //initialize mesh
        mTrunkMeshFilter = gameObject.AddComponent<MeshFilter>();
        mTrunkMesh = mTrunkMeshFilter.mesh;
        mTrunkMeshRenderer = gameObject.AddComponent<MeshRenderer>();
        mTrunkMeshRenderer.material = woodMaterial;
        
        int numVertebrae = Mathf.RoundToInt((float)trunkTotalThetaDegrees / DEGREES_PER_VERTICAL_SLICE);
        Vector3[] trunkSpine = GenerateTreeSpine(numVertebrae+1, new Vector3(0, 0, 0));

        float[] radii = new float[numVertebrae+1];
        for (int i = 0; i < radii.Length; i++)
        {
            radii[i] = 1.0f;
        }

        //mTrunkMeshFilter.mesh = CylinderGenerator.GenerateCurvedCylinder(trunkHeight, radii, cylinderResolution, numVertebrae, trunkSpine);


        for (int i = 0; i < 5; i++)
        {
            trunkSpine = GenerateTreeSpine(numVertebrae + 1, new Vector3(0, 1, 0)); radii = new float[numVertebrae + 1];

            float curRadii = 0.5f;
            for (int j = 0; j < radii.Length; j++)
            {

                curRadii += UnityEngine.Random.Range(0.035f, 0.04f);
                radii[j] = curRadii;
            }
            GameObject testBranch = new GameObject();
            testBranch.transform.position = transform.position;
            testBranch.transform.rotation = transform.rotation;
            MeshFilter testBranchMF = testBranch.AddComponent<MeshFilter>();
            testBranchMF.gameObject.AddComponent<MeshRenderer>().material = woodMaterial;
            testBranchMF.mesh = CylinderGenerator.GenerateCurvedCylinder(trunkHeight, radii, cylinderResolution, numVertebrae, trunkSpine); 
        }
    }

    private Vector3[] GenerateTreeSpine(int numVertebrae, Vector3 initalPosition)
    {
        Vector3[] spine = new Vector3[numVertebrae];

        //first vertebra will point upwards
        Vector3 spineVector = Vector3.up;

        float heightStep = trunkHeight / (float)numVertebrae;
        float angleStepDegrees = trunkTotalThetaDegrees / (float)numVertebrae;

        Vector3 currentVertebra = Vector3.zero;

        float forwardMod = UnityEngine.Random.Range(0.0f, 1.0f);
        float rightMod = 1.0f - forwardMod;

        int negateForward = UnityEngine.Random.Range(0, 2);
        if (negateForward == 0)
        {
            forwardMod *= -1.0f;
        }

        int negateRight = UnityEngine.Random.Range(0, 2);
        if (negateRight == 0)
        {
            rightMod *= -1.0f;
        }

        //Vector3 prevVertebra = new Vector3(0.0f, 0.0f, 0.0f);
        //Vector3 prevPrevVertebra = new Vector3(0.0f, 0.0f, 0.0f);
        float currentHeightStep = heightStep;
        for (int i = 0; i < numVertebrae; i++)
        {
            currentVertebra = Quaternion.AngleAxis(angleStepDegrees * rightMod, Vector3.right) * currentVertebra;
            currentVertebra = Quaternion.AngleAxis(angleStepDegrees * forwardMod, Vector3.forward) * currentVertebra;
            
            currentVertebra = new Vector3(currentVertebra.x, (heightStep * (float)i), currentVertebra.z);
            currentVertebra += initalPosition;


            spine[i] = currentVertebra;
            //prevPrevVertebra = prevVertebra;
            //prevVertebra = currentVertebra;
        }

        return spine;
    }
}
