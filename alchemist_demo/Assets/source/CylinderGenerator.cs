﻿using UnityEngine;
using System.Collections;

public static class CylinderGenerator
{
    //radialVerts is the number of vertices per circle in the cylinder
    //verticalVerts is the number of segments of the trunk... how many vertices are in one side egde of the cylinder
    public static Mesh GenerateCylinder(float worldHeight, float worldRadius, int radialVerts, int verticalVerts)
    {
        Mesh mesh = new Mesh();

        //extra vertex for wrap-around
        int numVerts = (radialVerts + 1) * (verticalVerts + 1);
        int numUVs = numVerts;

        //for one segment
        int numSideTris = radialVerts * verticalVerts * 2;
        int numCapTris = radialVerts - 2;

        //arrays
        Vector3[] verts = new Vector3[numVerts];
        Vector2[] uvs = new Vector2[numUVs];
        int[] tris = new int[(numSideTris + numCapTris * 2) * 3];

        //calculate step sizes
        float heightStep = worldHeight / verticalVerts;
        float angleStep = Utils.TWO_PI / radialVerts;
        float horizontalUVStep = 1.0f / radialVerts;
        float verticalUVStep = 1.0f / verticalVerts;

        //create sides
        for (int i = 0; i < (verticalVerts + 1); i++)
        {
            for (int j = 0; j < (radialVerts + 1); j++)
            {
                float newAngle = j * angleStep;

                //wrap-around verts -- if i is the 
                if (j == radialVerts)
                {
                    newAngle = 0.0f;
                }

                //set vertex position
                verts[i * (radialVerts + 1) + j] = new Vector3(worldRadius * Mathf.Cos(newAngle), i * heightStep, worldRadius * Mathf.Sin(newAngle));

                //set uvs
                uvs[i * (radialVerts + 1) + j] = new Vector2(j * horizontalUVStep, i * verticalUVStep);

                //set tris
                if (i == 0 || j >= radialVerts)
                {
                    continue;
                }
                else
                {
                    int baseIndex = numCapTris * 3 + ((i - 1) * (radialVerts * 6) + (j * 6));

                    //first tri
                    tris[baseIndex + 0] = i * (radialVerts + 1) + j;
                    tris[baseIndex + 1] = i * (radialVerts + 1) + j + 1;
                    tris[baseIndex + 2] = (i - 1) * (radialVerts + 1) + j;

                    //second tri
                    tris[baseIndex + 3] = (i - 1) * (radialVerts + 1) + j;
                    tris[baseIndex + 4] = i * (radialVerts + 1) + j + 1;
                    tris[baseIndex + 5] = (i - 1) * (radialVerts + 1) + j + 1;
                }
            }
        }

        //create caps
        bool leftSided = true;
        int leftIndex = 0;
        int middleIndex = 0;
        int rightIndex = 0;
        int topCapVertexOffset = numVerts - (radialVerts + 1);

        int bottomCapBaseIndex = 0;
        int topCapBaseIndex = 0;

        for (int i = 0; i < numCapTris; i++)
        {
            bottomCapBaseIndex = i * 3;
            topCapBaseIndex = (numCapTris + numSideTris) * 3 + (i * 3);

            if (i == 0)
            {
                middleIndex = 0;
                leftIndex = 1;
                rightIndex = (radialVerts + 1) - 2;
                leftSided = true;
            }
            else if (leftSided)
            {
                middleIndex = rightIndex;
                rightIndex--;
            }
            else
            {
                middleIndex = leftIndex;
                leftIndex++;
            }
            leftSided = !leftSided;

            //assign bottom tris
            tris[bottomCapBaseIndex + 0] = rightIndex;
            tris[bottomCapBaseIndex + 1] = middleIndex;
            tris[bottomCapBaseIndex + 2] = leftIndex;

            //assign top tris
            tris[topCapBaseIndex + 0] = topCapVertexOffset + leftIndex;
            tris[topCapBaseIndex + 1] = topCapVertexOffset + middleIndex;
            tris[topCapBaseIndex + 2] = topCapVertexOffset + rightIndex;
        }


        //set mesh data
        mesh.vertices = verts;
        mesh.uv = uvs;
        mesh.triangles = tris;

        //mesh.RecalculateNormals();
        NormalSolver.RecalculateNormals(mesh, 45);

        CalculateMeshTangents(mesh);

        return mesh;
    }

    // Recalculate mesh tangents
    // I found this on the internet (Unity forums?), I don't take credit for it.
    public static void CalculateMeshTangents(Mesh mesh)
    {
    
        //speed up math by copying the mesh arrays
        int[] triangles = mesh.triangles;
        Vector3[] vertices = mesh.vertices;
        Vector2[] uv = mesh.uv;
        Vector3[] normals = mesh.normals;
    
        //variable definitions
        int triangleCount = triangles.Length;
        int vertexCount = vertices.Length;
    
        Vector3[] tan1 = new Vector3[vertexCount];
        Vector3[] tan2 = new Vector3[vertexCount];
    
        Vector4[] tangents = new Vector4[vertexCount];
    
        for (long a = 0; a < triangleCount; a += 3)
        {
            long i1 = triangles[a + 0];
            long i2 = triangles[a + 1];
            long i3 = triangles[a + 2];
    
            Vector3 v1 = vertices[i1];
            Vector3 v2 = vertices[i2];
            Vector3 v3 = vertices[i3];
    
            Vector2 w1 = uv[i1];
            Vector2 w2 = uv[i2];
            Vector2 w3 = uv[i3];
    
            float x1 = v2.x - v1.x;
            float x2 = v3.x - v1.x;
            float y1 = v2.y - v1.y;
            float y2 = v3.y - v1.y;
            float z1 = v2.z - v1.z;
            float z2 = v3.z - v1.z;
    
            float s1 = w2.x - w1.x;
            float s2 = w3.x - w1.x;
            float t1 = w2.y - w1.y;
            float t2 = w3.y - w1.y;
    
            float r = 1.0f / (s1 * t2 - s2 * t1);
    
            Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
            Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);
    
            tan1[i1] += sdir;
            tan1[i2] += sdir;
            tan1[i3] += sdir;
    
            tan2[i1] += tdir;
            tan2[i2] += tdir;
            tan2[i3] += tdir;
        }
    
        for (long a = 0; a < vertexCount; ++a)
        {
            Vector3 n = normals[a];
            Vector3 t = tan1[a];
    
            //Vector3 tmp = (t - n * Vector3.Dot(n, t)).normalized;
            //tangents[a] = new Vector4(tmp.x, tmp.y, tmp.z);
            Vector3.OrthoNormalize(ref n, ref t);
            tangents[a].x = t.x;
            tangents[a].y = t.y;
            tangents[a].z = t.z;
    
            tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[a]) < 0.0f) ? -1.0f : 1.0f;
        }
    
        mesh.tangents = tangents;
    }

    public static Mesh GenerateTreeTrunk(float worldHeight, float[] worldRadius, int radialVerts, int verticalVerts)
    {
        Mesh mesh = new Mesh();

        //extra vertex for wrap-around
        int numVerts = (radialVerts + 1) * (verticalVerts + 1);
        int numUVs = numVerts;

        //for one segment
        int numSideTris = radialVerts * verticalVerts * 2;
        int numCapTris = radialVerts - 2;

        //arrays
        Vector3[] verts = new Vector3[numVerts];
        Vector2[] uvs = new Vector2[numUVs];
        int[] tris = new int[(numSideTris + numCapTris * 2) * 3];

        //calculate step sizes
        float heightStep = worldHeight / verticalVerts;
        float angleStep = Utils.TWO_PI / radialVerts;
        float horizontalUVStep = 1.0f / radialVerts;
        float verticalUVStep = 1.0f / verticalVerts;

        //create sides
        for (int i = 0; i < (verticalVerts + 1); i++)
        {
            for (int j = 0; j < (radialVerts + 1); j++)
            {
                float newAngle = j * angleStep;

                //wrap-around verts -- if i is the 
                if (j == radialVerts)
                {
                    newAngle = 0.0f;
                }

                //set vertex position
                verts[i * (radialVerts + 1) + j] = new Vector3(worldRadius[i] * Mathf.Cos(newAngle), i * heightStep, worldRadius[i] * Mathf.Sin(newAngle));

                //set uvs
                uvs[i * (radialVerts + 1) + j] = new Vector2(j * horizontalUVStep, i * verticalUVStep);

                //set tris
                if (i == 0 || j >= radialVerts)
                {
                    continue;
                }
                else
                {
                    int baseIndex = numCapTris * 3 + ((i - 1) * (radialVerts * 6) + (j * 6));

                    //first tri
                    tris[baseIndex + 0] = i * (radialVerts + 1) + j;
                    tris[baseIndex + 1] = i * (radialVerts + 1) + j + 1;
                    tris[baseIndex + 2] = (i - 1) * (radialVerts + 1) + j;

                    //second tri
                    tris[baseIndex + 3] = (i - 1) * (radialVerts + 1) + j;
                    tris[baseIndex + 4] = i * (radialVerts + 1) + j + 1;
                    tris[baseIndex + 5] = (i - 1) * (radialVerts + 1) + j + 1;
                }
            }
        }

        //create caps
        bool leftSided = true;
        int leftIndex = 0;
        int middleIndex = 0;
        int rightIndex = 0;
        int topCapVertexOffset = numVerts - (radialVerts + 1);

        int bottomCapBaseIndex = 0;
        int topCapBaseIndex = 0;

        for (int i = 0; i < numCapTris; i++)
        {
            bottomCapBaseIndex = i * 3;
            topCapBaseIndex = (numCapTris + numSideTris) * 3 + (i * 3);

            if (i == 0)
            {
                middleIndex = 0;
                leftIndex = 1;
                rightIndex = (radialVerts + 1) - 2;
                leftSided = true;
            }
            else if (leftSided)
            {
                middleIndex = rightIndex;
                rightIndex--;
            }
            else
            {
                middleIndex = leftIndex;
                leftIndex++;
            }
            leftSided = !leftSided;

            //assign bottom tris
            tris[bottomCapBaseIndex + 0] = rightIndex;
            tris[bottomCapBaseIndex + 1] = middleIndex;
            tris[bottomCapBaseIndex + 2] = leftIndex;

            //assign top tris
            tris[topCapBaseIndex + 0] = topCapVertexOffset + leftIndex;
            tris[topCapBaseIndex + 1] = topCapVertexOffset + middleIndex;
            tris[topCapBaseIndex + 2] = topCapVertexOffset + rightIndex;
        }


        //set mesh data
        mesh.vertices = verts;
        mesh.uv = uvs;
        mesh.triangles = tris;

        //mesh.RecalculateNormals();
        NormalSolver.RecalculateNormals(mesh, 45);

        CalculateMeshTangents(mesh);

        return mesh;
    }

    public static Mesh GenerateCurvedCylinder(float worldHeight, float[] worldRadius, int radialVerts, int verticalVerts, Vector3[] spine)
    {
        Mesh mesh = new Mesh();

        //extra vertex for wrap-around
        int numVerts = (radialVerts + 1) * (verticalVerts + 1);
        int numUVs = numVerts;

        //for one segment
        int numSideTris = radialVerts * verticalVerts * 2;
        int numCapTris = radialVerts - 2;

        //arrays
        Vector3[] verts = new Vector3[numVerts];
        Vector2[] uvs = new Vector2[numUVs];
        int[] tris = new int[(numSideTris + numCapTris * 2) * 3];

        //calculate step sizes
        float heightStep = worldHeight / verticalVerts;
        float angleStep = Utils.TWO_PI / radialVerts;
        float horizontalUVStep = 1.0f / radialVerts;
        float verticalUVStep = 1.0f / verticalVerts;

        //create sides
        for (int i = 0; i < (verticalVerts + 1); i++)
        {
            Vector3 planeNormal = Vector3.zero;
            //if (i == 0)
            //{
                planeNormal = spine[i].normalized;
            //}
            //else
            //{
            //    planeNormal = Vector3.Cross((spine[i] - spine[i - 1]).normalized, Vector3.up);
            //}

            for (int j = 0; j < (radialVerts + 1); j++)
            {
                float newAngle = j * angleStep;

                //wrap-around verts -- if i is the 
                if (j == radialVerts)
                {
                    newAngle = 0.0f;
                }

                //set vertex position
                Vector3 newVec = new Vector3(worldRadius[i] * Mathf.Cos(newAngle), 0.0f, worldRadius[i] * Mathf.Sin(newAngle));
                newVec = newVec.magnitude * Vector3.Cross(newVec, planeNormal);
                newVec = newVec.normalized;
                newVec *= worldRadius[i];
                verts[i * (radialVerts + 1) + j] = newVec;
                verts[i * (radialVerts + 1) + j] += spine[i];


                //set uvs
                uvs[i * (radialVerts + 1) + j] = new Vector2(j * horizontalUVStep, i * verticalUVStep);

                //set tris
                if (i == 0 || j >= radialVerts)
                {
                    continue;
                }
                else
                {
                    int baseIndex = numCapTris * 3 + ((i - 1) * (radialVerts * 6) + (j * 6));

                    //first tri
                    tris[baseIndex + 0] = i * (radialVerts + 1) + j;
                    tris[baseIndex + 1] = i * (radialVerts + 1) + j + 1;
                    tris[baseIndex + 2] = (i - 1) * (radialVerts + 1) + j;

                    //second tri
                    tris[baseIndex + 3] = (i - 1) * (radialVerts + 1) + j;
                    tris[baseIndex + 4] = i * (radialVerts + 1) + j + 1;
                    tris[baseIndex + 5] = (i - 1) * (radialVerts + 1) + j + 1;
                }
            }
        }

        //create caps
        bool leftSided = true;
        int leftIndex = 0;
        int middleIndex = 0;
        int rightIndex = 0;
        int topCapVertexOffset = numVerts - (radialVerts + 1);

        int bottomCapBaseIndex = 0;
        int topCapBaseIndex = 0;

        for (int i = 0; i < numCapTris; i++)
        {
            bottomCapBaseIndex = i * 3;
            topCapBaseIndex = (numCapTris + numSideTris) * 3 + (i * 3);

            if (i == 0)
            {
                middleIndex = 0;
                leftIndex = 1;
                rightIndex = (radialVerts + 1) - 2;
                leftSided = true;
            }
            else if (leftSided)
            {
                middleIndex = rightIndex;
                rightIndex--;
            }
            else
            {
                middleIndex = leftIndex;
                leftIndex++;
            }
            leftSided = !leftSided;

            //assign bottom tris
            tris[bottomCapBaseIndex + 0] = rightIndex;
            tris[bottomCapBaseIndex + 1] = middleIndex;
            tris[bottomCapBaseIndex + 2] = leftIndex;

            //assign top tris
            tris[topCapBaseIndex + 0] = topCapVertexOffset + leftIndex;
            tris[topCapBaseIndex + 1] = topCapVertexOffset + middleIndex;
            tris[topCapBaseIndex + 2] = topCapVertexOffset + rightIndex;
        }


        //set mesh data
        mesh.vertices = verts;
        mesh.uv = uvs;
        mesh.triangles = tris;

        //mesh.RecalculateNormals();
        NormalSolver.RecalculateNormals(mesh, 45);

        CalculateMeshTangents(mesh);

        return mesh;
    }

}