﻿using UnityEngine;
using System.Collections;

public class Mushroom : MonoBehaviour
{
    public enum ShroomStage
    {
        INITIAL_CAP_BUDDING = 0,//cap grows in the ground, rounded
        STALK_FORMATION,//stalk grows, elevating cap on top of it. cap begins budding outward and upward, revealing gills (ridges underneath cap).
        FINAL_CAP_BUDDING//cap buds into more flattened shape
    }
    private ShroomStage currentStage;
    private float[] stageAgePercentages = new float[]{  0.2f,   //INITIAL_CAP_BUDDING
                                                        0.4f,   //STALK_FORMATION
                                                        0.4f }; //FINAL_CAP_BUDDING

    [Tooltip("Grow the tree in real time, or all at once")]
    public bool realtime;

    [Tooltip("how long will it take to grow fully?")]
    public float ageSeconds;
    private float ageTimer;

    public Material material;

    private MeshFilter mMeshFilter;
    private MeshRenderer mMeshRenderer;
    
    private float initialScale;
    private float finalScale;

    private const float heightPercentBelowGround = 0.35f;
    private float heightBelowGround;

    void Awake()
    {
        ageTimer = 0.0f;
        initialScale = 1.0f;
        finalScale = 20.0f;

        if (!(mMeshFilter = GetComponent<MeshFilter>()))
        {
            mMeshFilter = gameObject.AddComponent<MeshFilter>();
        }
        if (!(mMeshRenderer = GetComponent<MeshRenderer>()))
        {
            mMeshRenderer = gameObject.AddComponent<MeshRenderer>();
        }

        int numBoneVerts = 64;
        float sphere_radius = 0.1f;
        float sphere_diameter = sphere_radius*2.0f;
        heightBelowGround = heightPercentBelowGround * sphere_diameter;

        float[] radii = new float[numBoneVerts+1];

        float currentHeight = 0.0f;
        int capI = 0;//which i starts the cap?

        Vector3[] mushroomSpine = new Vector3[numBoneVerts + 1];
        for (int i = 0; i < mushroomSpine.Length; i++)
        {
            mushroomSpine[i] = new Vector3(0.0f, -heightBelowGround/2 + (Mathf.Lerp(0.0f, sphere_diameter, (float)i / (float)(mushroomSpine.Length))));
            if (mushroomSpine[i].y <= (heightBelowGround))
            {
                capI = i;
            }
        }

        for (int i = 0; i < radii.Length; i++)
        {
            currentHeight = (-sphere_radius + (sphere_diameter * ((float)(i) / (radii.Length-1))));
            if (i > capI)
            {
                radii[i] = Mathf.Sqrt((sphere_radius * sphere_radius) - (currentHeight * currentHeight));
            }
            else
            {
                radii[i] = 0.025f;
            }
        }

        mMeshFilter.mesh = CylinderGenerator.GenerateCurvedCylinder(1.0f, radii, 32, numBoneVerts, mushroomSpine);
        mMeshRenderer.material = material;
    }

    void FixedUpdate()
    {
        if (ageTimer < ageSeconds)
        {
            ageTimer += Time.deltaTime;
            float currentScale = Mathf.Lerp(initialScale, finalScale, ageTimer / ageSeconds);
            gameObject.transform.localScale = new Vector3(currentScale, currentScale, currentScale);
        }
    }
}
